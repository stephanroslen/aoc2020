#include <gtest/gtest.h>

#include <numeric>

#include <baselib/Algorithm.h>

TEST(AlgorithmCountClassification, Simple) {
  const std::array<int, 10> data{0, 3, 2, 0, 1, 1, 2, 3, 0, 1};
  const auto result{baselib::CountClassification<4>(std::cbegin(data), std::cend(data), [](const int v) { return v; })};
  const std::array<ssize_t, 4> expectedResult{3, 3, 2, 2};
  EXPECT_EQ(result, expectedResult);
}

TEST(AlgorithmCountClassification, Modulus) {
  std::array<int, 20> data{};
  std::iota(std::begin(data), std::end(data), 0);
  const auto result{
      baselib::CountClassification<3>(std::cbegin(data), std::cend(data), [](const int v) { return v % 3; })};
  const std::array<ssize_t, 3> expectedResult{7, 7, 6};
  EXPECT_EQ(result, expectedResult);
}