find_package(GTest REQUIRED)

add_executable(AlgorithmTests AlgorithmTests.cpp)
target_link_libraries(AlgorithmTests baselib GTest::Main)
set_target_properties(AlgorithmTests PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/tests")
gtest_discover_tests(AlgorithmTests)