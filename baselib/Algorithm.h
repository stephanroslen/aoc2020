#pragma once

#include <algorithm>
#include <array>
#include <iterator>

namespace baselib {

template<size_t N, typename Iter, typename Classifier>
constexpr auto CountClassification(Iter begin, Iter end, Classifier&& classifier) {
  using CType = typename std::iterator_traits<Iter>::difference_type;
  std::array<CType, N> result{};
  std::fill(std::begin(result), std::end(result), CType{0});
  while (begin != end) {
    ++result.at(classifier(*begin++));
  }
  return result;
}

} // namespace baselib