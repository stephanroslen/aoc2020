#pragma once

namespace baselib {

template<typename T>
struct IntegerCoord {
  T x;
  T y;

  constexpr IntegerCoord& operator+=(const IntegerCoord& other) noexcept {
    x += other.x;
    y += other.y;
    return *this;
  }

  constexpr IntegerCoord operator+(const IntegerCoord& other) const noexcept {
    IntegerCoord result{*this};
    result += other;
    return result;
  }

  constexpr IntegerCoord operator*(const T n) const noexcept { return {x * n, y * n}; }

  constexpr T ManDist(const IntegerCoord& other) const noexcept {
    T dx = other.x - x;
    T dy = other.y - y;
    return ((dx < 0) ? -dx : dx) + ((dy < 0) ? -dy : dy);
  }

  constexpr size_t Hash() const noexcept {
    std::hash<T> hasher{};
    size_t result{hasher(x)};
    return ((result << 10) ^ (result >> 1) ^ hasher(y)) + 12345;
  }

  bool operator==(const IntegerCoord& rhs) const noexcept { return std::tie(x, y) == std::tie(rhs.x, rhs.y); }
  bool operator!=(const IntegerCoord& rhs) const noexcept { return !(rhs == *this); }
};

using IntCoord = IntegerCoord<int>;

} // namespace baselib

namespace std {

template<typename T>
struct hash<baselib::IntegerCoord<T>> {
  size_t operator()(const baselib::IntegerCoord<T>& v) const noexcept { return v.Hash(); }
};

} // namespace std