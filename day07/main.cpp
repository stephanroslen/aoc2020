#include <iostream>
#include <queue>
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>

#include <fmt/core.h>

using BagColor    = std::string;
using BagIndex    = unsigned int;
using BagCount    = unsigned int;
using BagCounter  = std::vector<std::pair<BagIndex, BagCount>>;
using BagBackrefs = std::vector<BagIndex>;
using BagMap      = std::vector<std::tuple<BagCounter, BagBackrefs>>;
using BagIndexer  = std::unordered_map<BagColor, BagIndex>;

int main() {
  const auto [bagMap, bagIndexer] = []() {
    std::tuple<BagMap, BagIndexer> result;
    auto& [bagMapResult, bagIndexerResult] = result;
    const auto getBagIndex{[&bagMapResult, &bagIndexerResult](const BagColor& color) -> BagIndex {
      const auto iter = bagIndexerResult.find(color);
      if (std::cend(bagIndexerResult) != iter) {
        return iter->second;
      } else {
        const BagIndex res{static_cast<BagIndex>(bagIndexerResult.size())};
        bagIndexerResult.emplace(color, res);
        bagMapResult.emplace_back();
        return res;
      }
    }};
    std::string line;
    while (std::cin) {
      std::getline(std::cin, line);
      std::string_view alias{line};
      const auto bagsPos = alias.find("bags");
      if (bagsPos == std::string_view::npos) {
        break;
      }
      const BagColor inColor{alias.substr(0, bagsPos - 1)};
      const BagIndex inIndex{getBagIndex(inColor)};
      std::string_view inResidual = alias.substr(bagsPos + 12);
      if (!inResidual.starts_with(" no")) {
        while (!inResidual.empty()) {
          const auto commaPos            = inResidual.find_first_of(",.");
          const std::string_view outElem = inResidual.substr(1, commaPos - 1);
          inResidual.remove_prefix(commaPos + 1);
          const auto splitPos = outElem.find(" ");
          const auto bagPos   = outElem.find("bag");
          const size_t number = std::stoul(std::string{outElem.substr(0, splitPos)});
          const BagColor outColor{outElem.substr(splitPos + 1, bagPos - splitPos - 2)};
          const BagIndex outIndex{getBagIndex(outColor)};
          auto& [bagCount, bagBackref]       = bagMapResult.at(inIndex);
          auto& [outBagCount, outBagBackref] = bagMapResult.at(outIndex);
          bagCount.emplace_back(outIndex, number);
          outBagBackref.emplace_back(inIndex);
        }
      }
    }
    return result;
  }();

  const auto shinyGoldIndex = bagIndexer.at("shiny gold");

  const BagCount part1{[&]() {
    std::unordered_set<BagIndex> indexSet{};
    const auto countBackRefs{
        [&](auto&& rec, const BagIndex& bagIndex, std::unordered_set<BagIndex>& collected) -> void {
          const auto& [bag, bagBackref] = bagMap.at(bagIndex);
          for (const auto& entryIndex : bagBackref) {
            collected.emplace(entryIndex);
            rec(rec, entryIndex, collected);
          }
        }};
    countBackRefs(countBackRefs, shinyGoldIndex, indexSet);
    return static_cast<BagCount>(indexSet.size());
  }()};
  fmt::print("{}\n", part1);

  const auto countSubBags{[&](auto&& rec, const BagIndex& bagIndex) -> BagCount {
    BagCount result{1};
    const auto& [bag, bagBackref] = bagMap.at(bagIndex);
    for (const auto& [entryIndex, entryCount] : bag) {
      result += entryCount * rec(rec, entryIndex);
    }
    return result;
  }};
  const BagCount part2{countSubBags(countSubBags, shinyGoldIndex) - 1};
  fmt::print("{}\n", part2);

  return 0;
}