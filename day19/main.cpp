#include <algorithm>
#include <cassert>
#include <future>
#include <iostream>
#include <numeric>
#include <unordered_map>
#include <unordered_set>

#include <fmt/core.h>

struct NTRule {
  std::vector<int> nts{};

  NTRule(const std::vector<int>& nts_) : nts{nts_} {}
};

struct TRule {
  char c{};

  TRule(char c_) : c{c_} {}
};

struct Rules {
  std::vector<NTRule> ntRules{};
  std::vector<TRule> tRules{};
};

struct Parser {
  std::unordered_map<int, Rules> ruleMap{};

  void ToCNF() {
    std::vector<std::vector<int>> tripleEntries{};
    for (auto& [idx, rules] : ruleMap) {
      auto& ntRules = rules.ntRules;
      auto& tRules  = rules.tRules;
      auto iter     = std::begin(ntRules);
      std::vector<int> singleEntries{};
      while (iter != std::end(ntRules)) {
        if (iter->nts.size() == 1) {
          singleEntries.emplace_back(*std::begin(iter->nts));
          iter = ntRules.erase(iter);
        } else if (iter->nts.size() == 3) {
          tripleEntries.emplace_back(iter->nts);
          tripleEntries.back().emplace_back(idx);
          iter = ntRules.erase(iter);
        } else {
          ++iter;
        }
      }
      for (const int& singleEntry : singleEntries) {
        const auto& otherNTRules{ruleMap.at(singleEntry).ntRules};
        const auto& otherTRules{ruleMap.at(singleEntry).tRules};
        ntRules.insert(std::end(ntRules), std::begin(otherNTRules), std::end(otherNTRules));
        tRules.insert(std::end(tRules), std::begin(otherTRules), std::end(otherTRules));
      }
    }
    for (std::vector<int>& tripleEntry : tripleEntries) {
      int oldIdx{tripleEntry.back()};
      tripleEntry.pop_back();
      int thirdIdx{tripleEntry.back()};
      tripleEntry.pop_back();
      int newIdx{static_cast<int>(ruleMap.size())};
      assert(!ruleMap.contains(newIdx));
      auto& newEntry = ruleMap[newIdx];
      newEntry.ntRules.emplace_back(tripleEntry);
      ruleMap[oldIdx].ntRules.emplace_back(std::vector<int>{newIdx, thirdIdx});
    }
  }

  bool Accepts(const std::string& s) const {
    std::vector<std::vector<std::unordered_set<int>>> parseMatrix{};
    const size_t size{s.size()};
    parseMatrix.emplace_back();
    auto& firstRow = parseMatrix[0];
    for (size_t i{0}; i < size; ++i) {
      std::unordered_set<int> validRules{};
      const char c{s[i]};
      for (const auto& [idx, rules] : ruleMap) {
        for (const auto& tRule : rules.tRules) {
          if (c == tRule.c) {
            validRules.emplace(idx);
          }
        }
      }
      firstRow.emplace_back(std::move(validRules));
    }
    for (size_t j{1}; j < size; ++j) {
      parseMatrix.emplace_back();
      auto& currentRow = parseMatrix[j];
      for (size_t i{0}; i < size - j; ++i) {
        std::unordered_set<int> validRules{};
        for (size_t k{0}; k < j; ++k) {
          for (const auto& [idx, rules] : ruleMap) {
            for (const auto& ntRule : rules.ntRules) {
              if (parseMatrix[k][i].contains(ntRule.nts[0])
                  && parseMatrix[j - k - 1][i + k + 1].contains(ntRule.nts[1])) {
                validRules.emplace(idx);
              }
            }
          }
        }
        currentRow.emplace_back(std::move(validRules));
      }
    }

    return parseMatrix[size - 1][0].contains(0);
  }
};

int main() {
  const auto& [parser, candidates]{[]() {
    std::tuple<Parser, std::vector<std::string>> result{};
    auto& [rParser, rCandidates] = result;
    int stage{0};
    std::string line{};

    while (std::getline(std::cin, line)) {
      if (line.empty()) {
        stage += 1;
        continue;
      }
      if (stage == 0) {
        std::string_view sv{line};
        const size_t locCol{sv.find(':')};
        int idx{std::stoi(std::string{sv.substr(0, locCol)})};
        Rules& rules = rParser.ruleMap[idx];
        sv.remove_prefix(locCol + 2);
        if (sv[0] == '"') {
          rules.tRules.emplace_back(sv[1]);
        } else {
          while (0 != sv.size()) {
            size_t locEnd{sv.find('|')};
            if (locEnd == std::string_view::npos) {
              locEnd = sv.size();
            }
            std::vector<int> nts{};
            std::string_view locSV{sv.substr(0, locEnd)};
            while (locSV.ends_with(' ')) {
              locSV.remove_suffix(1);
            }
            while (locSV.size() > 0) {
              while (locSV.starts_with(' ')) {
                locSV.remove_prefix(1);
              }
              size_t locSym{locSV.find(' ')};
              if (locSym == std::string_view::npos) {
                locSym = locSV.size();
              }
              nts.emplace_back(std::stoi(std::string{locSV.substr(0, locSym)}));
              locSV.remove_prefix(std::min(locSym + 1, locSV.size()));
            }
            assert(nts.size() <= 3);
            sv.remove_prefix(std::min(locEnd + 2, sv.size()));
            rules.ntRules.emplace_back(nts);
          }
        }
      } else {
        rCandidates.emplace_back(line);
      }
    }
    return result;
  }()};

  const auto solver{[&](const Parser& p) {
    std::vector<std::future<bool>> futures{};
    futures.reserve(candidates.size());
    std::transform(std::cbegin(candidates),
                   std::cend(candidates),
                   std::back_inserter(futures),
                   [&](const std::string& candidate) -> std::future<bool> {
                     return std::async(std::launch::async, [&]() { return p.Accepts(candidate); });
                   });
    return std::transform_reduce(std::begin(futures),
                                 std::end(futures),
                                 size_t{0},
                                 std::plus{},
                                 [](std::future<bool>& fut) -> size_t { return fut.get() ? size_t{1} : size_t{0}; });
  }};

  const auto parser1{[&](){
    auto result{parser};
    result.ToCNF();
    return result;
  }()};
  fmt::print("{}\n", solver(parser1));

  const auto parser2{[&](){
    auto result{parser};
    result.ruleMap.at(8).ntRules.emplace_back(std::vector<int>{42, 8});
    result.ruleMap.at(11).ntRules.emplace_back(std::vector<int>{42, 11, 31});
    result.ToCNF();
    return result;
  }()};
  fmt::print("{}\n", solver(parser2));

  return 0;
}