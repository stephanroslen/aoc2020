#include <algorithm>
#include <bitset>
#include <climits>
#include <iostream>
#include <queue>
#include <string_view>
#include <unordered_map>
#include <variant>
#include <vector>

#include <fmt/core.h>

using Address = std::bitset<36>;
using Value   = std::bitset<36>;
using Memory  = std::unordered_map<Address, Value>;

struct Mask {
  Value maskMask{};
  Value maskValue{};

  Value DoMask(const Value input) const noexcept {
    Value Ones  = maskMask & maskValue;
    Value Zeros = maskMask & ~maskValue;

    return ~(~(input | Ones) | Zeros);
  }

  template<typename Morphism>
  void IterateAddresses(Address input, Morphism&& morphism) const noexcept {
    input |= maskValue;
    const auto invMask = ~maskMask;

    const auto apply{[&](auto&& rec, Address vInvMask, Address in) noexcept -> void {
      if (vInvMask.none()) {
        morphism(in);
      } else {
        const auto maskV = vInvMask.to_ulong();
        static_assert(sizeof(decltype(maskV)) * CHAR_BIT >= 36);
        const auto maskVCLZ = __builtin_clzl(maskV);
        const auto val = (sizeof(decltype(maskV)) * CHAR_BIT) - maskVCLZ - 1;
        vInvMask[val] = false;
        in[val] = false;
        rec(rec, vInvMask, in);
        in[val] = true;
        rec(rec, vInvMask, in);
      }
    }};
    apply(apply, invMask, input);
  }
};

struct Machine {
  Mask mask{};
  Memory memory{};
};

struct MaskInstruction {
  Mask mask{};

  MaskInstruction() = default;
  explicit MaskInstruction(std::string_view sv) noexcept {
    sv.remove_prefix(7);
    for (const auto c : sv) {
      mask.maskMask <<= 1;
      mask.maskValue <<= 1;
      if (c == 'X') {
        mask.maskMask[0]  = 0;
        mask.maskValue[0] = 0;
      } else if (c == '0') {
        mask.maskMask[0]  = 1;
        mask.maskValue[0] = 0;
      } else {
        mask.maskMask[0]  = 1;
        mask.maskValue[0] = 1;
      }
    }
  }

  void Execute1(Machine& mach) const noexcept { mach.mask = mask; };

  void Execute2(Machine& mach) const noexcept { mach.mask = mask; };
};

struct WriteInstruction {
  Address address{0};
  Value value{0};

  WriteInstruction() = default;
  WriteInstruction(std::string_view sv) {
    sv.remove_prefix(4);
    size_t svLen               = sv.find(']');
    std::string_view addressSV = sv.substr(0, svLen);
    sv.remove_prefix(svLen + 4);

    address = std::stoi(std::string{addressSV});
    value   = std::stoi(std::string{sv});
  };

  void Execute1(Machine& mach) const noexcept { mach.memory[address] = mach.mask.DoMask(value); };

  void Execute2(Machine& mach) const noexcept {
    mach.mask.IterateAddresses(address, [&](const Address inAddr) { mach.memory[inAddr] = value; });
  };
};

struct Instruction {
  std::variant<MaskInstruction, WriteInstruction> instruction;

  friend std::istream& operator>>(std::istream& is, Instruction& inst) {
    std::string line;
    if (std::getline(is, line)) {
      std::string_view lineSV{line};
      if (line.starts_with("mask = ")) {
        inst.instruction = MaskInstruction{lineSV};
      } else {
        inst.instruction = WriteInstruction{lineSV};
      }
    }
    return is;
  }

  void Execute1(Machine& mach) const noexcept {
    std::visit([&](const auto& instr) { instr.Execute1(mach); }, instruction);
  };

  void Execute2(Machine& mach) const noexcept {
    std::visit([&](const auto& instr) { instr.Execute2(mach); }, instruction);
  };
};

int main() {
  const auto instructions{[]() {
    std::vector<Instruction> result{};
    std::copy(std::istream_iterator<Instruction>{std::cin},
              std::istream_iterator<Instruction>{},
              std::back_inserter(result));
    return result;
  }()};

  const auto solver{[&](auto&& instExec) {
    size_t result{0};
    Machine machine{};
    for (const auto& instr : instructions) {
      instExec(instr, machine);
    }
    for (const auto& [addr, val] : machine.memory) {
      result += static_cast<size_t>(val.to_ulong());
    }
    return result;
  }};

  fmt::print("{}\n", solver([](const Instruction& instr, Machine& machine) { instr.Execute1(machine); }));
  fmt::print("{}\n", solver([](const Instruction& instr, Machine& machine) { instr.Execute2(machine); }));

  return 0;
}