#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <fmt/core.h>

using Row   = std::vector<bool>;
using Image = std::vector<std::vector<bool>>;

struct Tile {
  int id{0};

  Image data{};

  size_t Count() const {
    size_t result{0};
    for (const auto& row : data) {
      for (const auto& point : row) {
        if (point) {
          result += 1;
        }
      }
    }
    return result;
  }

  bool IsAt(const Tile& otherTile, size_t x, size_t y) const {
    for (size_t iY{0}; iY < otherTile.data.size(); ++iY) {
      const auto& row = otherTile.data[iY];
      for (size_t iX{0}; iX < row.size(); ++iX) {
        if (otherTile.data[iY][iX] && !data[y + iY][x + iX]) {
          return false;
        }
      }
    }
    return true;
  }

  void RuleOut(const Tile& otherTile, size_t x, size_t y) {
    for (size_t iY{0}; iY < otherTile.data.size(); ++iY) {
      const auto& row = otherTile.data[iY];
      for (size_t iX{0}; iX < row.size(); ++iX) {
        if (otherTile.data[iY][iX] && data[y + iY][x + iX]) {
          data[y + iY][x + iX] = false;
        }
      }
    }
  }

  bool Occurs(const Tile& otherTile) const {
    size_t xSz{data.front().size()};
    size_t ySz{data.size()};

    size_t xSzO{otherTile.data.front().size()};
    size_t ySzO{otherTile.data.size()};

    for (size_t iY{0}; iY < ySz - ySzO + 1; ++iY) {
      for (size_t iX{0}; iX < xSz - xSzO + 1; ++iX) {
        if (IsAt(otherTile, iX, iY)) {
          return true;
        }
      }
    }

    return false;
  }

  void ClearOccurences(const Tile& otherTile) {
    size_t xSz{data.front().size()};
    size_t ySz{data.size()};

    size_t xSzO{otherTile.data.front().size()};
    size_t ySzO{otherTile.data.size()};

    for (size_t iY{0}; iY < ySz - ySzO + 1; ++iY) {
      for (size_t iX{0}; iX < xSz - xSzO + 1; ++iX) {
        if (IsAt(otherTile, iX, iY)) {
          RuleOut(otherTile, iX, iY);
        }
      }
    }
  }

  template<typename Morphism>
  void EachEdge(Morphism&& morphism) const {
    const auto execute{[&](Row edge) {
      morphism(edge);
      std::reverse(std::begin(edge), std::end(edge));
      morphism(edge);
    }};
    execute(TopEdge());
    execute(BottomEdge());
    execute(LeftEdge());
    execute(RightEdge());
  }

  void FlipTopDown() { std::reverse(std::begin(data), std::end(data)); }

  void RotateLeft() {
    const auto oldData{data};
    for (size_t ix{0}; ix < data.size(); ++ix) {
      for (size_t iy{0}; iy < data.size(); ++iy) {
        data[data.size() - 1 - ix][iy] = oldData[iy][ix];
      }
    }
  }

  template<typename Morphism>
  std::tuple<bool, Tile> EachOrientation(Morphism&& morphism) const {
    Tile d{*this};
    bool success{morphism(d)};
    int tryNum{0};
    while (!success && ++tryNum < 8) {
      if (tryNum == 4) {
        d.FlipTopDown();
      } else {
        d.RotateLeft();
      }
      success = morphism(d);
    }
    return {success, d};
  }

  Row TopEdge() const { return data.front(); }

  Row BottomEdge() const { return data.back(); }

  Row LeftEdge() const {
    Row edge{};
    for (const auto& row : data) {
      edge.emplace_back(row.front());
    }
    return edge;
  }

  Row RightEdge() const {
    Row edge{};
    for (const auto& row : data) {
      edge.emplace_back(row.back());
    }
    return edge;
  }

  std::unordered_set<Row> EdgeSet() const {
    std::unordered_set<Row> result{};
    EachEdge([&](const std::vector<bool>& edge) { result.emplace(edge); });
    return result;
  }

  void RemoveBorder() {
    data.pop_back();
    data.erase(std::begin(data));
    for (auto& row : data) {
      row.pop_back();
      row.erase(std::begin(row));
    }
  }

  Tile MergeHor(const Tile& other) const {
    Tile result{};
    result.id = -1;
    assert(data.size() == other.data.size());
    for (size_t i{0}; i < data.size(); ++i) {
      result.data.emplace_back();
      for (size_t iC{0}; iC < data[i].size(); ++iC) {
        result.data.back().emplace_back(data[i][iC]);
      }
      for (size_t iC{0}; iC < other.data[i].size(); ++iC) {
        result.data.back().emplace_back(other.data[i][iC]);
      }
    }
    return result;
  }

  Tile MergeVert(const Tile& other) const {
    Tile result{};
    result.id = -1;
    assert(data[0].size() == other.data[0].size());
    for (const auto& row : data) {
      result.data.emplace_back(row);
    }
    for (const auto& row : other.data) {
      result.data.emplace_back(row);
    }
    return result;
  }

  std::istream& ReadFrom(std::istream& is) {
    std::string line{};
    if (std::getline(is, line)) {
      if (line.empty()) {
        std::getline(is, line);
      }
      assert(line.starts_with("Tile "));
      id = std::stoi(line.substr(5, line.size() - 6));
      data.clear();
      std::getline(is, line);
      assert(is);
      int width{static_cast<int>(line.size())};
      int i{0};
      const auto insertLine{[&]() {
        std::vector<bool> curr{};
        for (const auto& c : line) {
          curr.emplace_back(c == '#');
        }
        data.emplace_back(curr);
      }};
      insertLine();
      while (++i < width && std::getline(is, line)) {
        if (line.empty()) {
          break;
        }
        insertLine();
      }
    }
    return is;
  }

  bool operator==(const Tile& rhs) const { return id == rhs.id; }
  bool operator!=(const Tile& rhs) const { return !(rhs == *this); }

  size_t Hash() const { return std::hash<decltype(id)>{}(id); }

  friend std::istream& operator>>(std::istream& is, Tile& tile) { return tile.ReadFrom(is); }
};

namespace std {
template<>
struct hash<Tile> {
  size_t operator()(const Tile& tile) const { return tile.Hash(); }
};
} // namespace std

using TileRow = std::vector<Tile>;
using TileMap = std::vector<TileRow>;

int main() {
  std::vector<Tile> tiles{};
  std::copy(std::istream_iterator<Tile>{std::cin}, std::istream_iterator<Tile>{}, std::back_inserter(tiles));
  std::unordered_map<Tile, std::unordered_set<std::vector<bool>>> edges{};
  std::unordered_map<Tile, int> sharedEdges{};
  for (const Tile& tile : tiles) {
    edges.emplace(tile, tile.EdgeSet());
  }

  size_t part1{1};

  Tile someCornerTile{};

  for (const auto& [tile0, edges0] : edges) {
    for (const auto& [tile1, edges1] : edges) {
      if (tile0 == tile1) {
        continue;
      }
      for (const auto& edge : edges0) {
        if (edges1.contains(edge)) {
          sharedEdges[tile0] += 1;
          break;
        }
      }
    }
    if (sharedEdges.at(tile0) == 2) {
      part1 *= tile0.id;
      someCornerTile = tile0;
    }
  }

  fmt::print("{}\n", part1);

  const auto [found, topLeftCornerTile] = someCornerTile.EachOrientation([&](const Tile& tile) -> bool {
    const auto rightEdge  = tile.RightEdge();
    const auto bottomEdge = tile.BottomEdge();
    bool foundRight{false};
    bool foundBottom{false};
    for (const auto& [otherTile, otherEdges] : edges) {
      if (tile == otherTile) {
        continue;
      }
      if (otherEdges.contains(rightEdge)) {
        foundRight = true;
      } else if (otherEdges.contains(bottomEdge)) {
        foundBottom = true;
      }
    }
    return foundRight && foundBottom;
  });

  assert(found);

  TileMap tileMap{TileRow{topLeftCornerTile}};

  const auto removeTile{
      [&](const Tile& element) { tiles.erase(std::remove(std::begin(tiles), std::end(tiles), element)); }};

  removeTile(topLeftCornerTile);

  const auto completeRow{[&]() {
    bool foundNextTile{false};
    do {
      Tile& element = tileMap.back().back();
      Row rightEdge = element.RightEdge();
      foundNextTile = false;
      for (const Tile& otherTile : tiles) {
        const auto [success, otherOrientedTile] =
            otherTile.EachOrientation([&](const Tile& candidate) -> bool { return rightEdge == candidate.LeftEdge(); });
        if (success) {
          foundNextTile = true;
          tileMap.back().emplace_back(otherOrientedTile);
          removeTile(otherOrientedTile);
          break;
        }
      }
    } while (foundNextTile);
  }};

  const auto startNextRow{[&]() {
    Tile& element  = tileMap.back().front();
    Row bottomEdge = element.BottomEdge();
    for (const Tile& otherTile : tiles) {
      const auto [success, otherOrientedTile] =
          otherTile.EachOrientation([&](const Tile& candidate) -> bool { return bottomEdge == candidate.TopEdge(); });
      if (success) {
        tileMap.emplace_back(TileRow{otherOrientedTile});
        removeTile(otherOrientedTile);
        break;
      }
    }
  }};

  completeRow();

  while (!tiles.empty()) {
    startNextRow();
    completeRow();
  }

  for (TileRow& tileRow : tileMap) {
    for (Tile& tile : tileRow) {
      tile.RemoveBorder();
    }
    Tile& first = tileRow.front();
    bool skip{true};
    for (Tile& tile : tileRow) {
      if (skip) {
        skip = false;
        continue;
      }
      first = first.MergeHor(tile);
    }
    tileRow.erase(std::next(std::begin(tileRow)), std::end(tileRow));
  }

  bool skip{true};
  for (TileRow& tileRow : tileMap) {
    if (skip) {
      skip = false;
      continue;
    }
    tileMap.front().front() = tileMap.front().front().MergeVert(tileRow.front());
  }
  tileMap.erase(std::next(std::begin(tileMap)), std::end(tileMap));

  Tile image{tileMap.front().front()};

  Tile monster{};
  constexpr std::array<std::string_view, 3> rawMonster{
      {"                  # ", "#    ##    ##    ###", " #  #  #  #  #  #   "}};
  for (const auto& line : rawMonster) {
    monster.data.emplace_back();
    for (const char& c : line) {
      monster.data.back().emplace_back(c == '#');
    }
  };

  auto [success, orientedImage] = image.EachOrientation([&](const Tile& tile) { return tile.Occurs(monster); });

  orientedImage.ClearOccurences(monster);

  fmt::print("{}\n", orientedImage.Count());

  return 0;
}