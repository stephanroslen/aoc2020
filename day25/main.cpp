#include <iostream>

#include <fmt/core.h>

namespace {

constexpr size_t p{20201227};
constexpr size_t base{7};

constexpr size_t calcLoopSize(size_t v) noexcept {
  size_t residue{0};
  while (v != base) {
    v *= base;
    v %= p;
    residue += 1;
  }
  return p - residue;
}

constexpr size_t baseToV(size_t v) noexcept {
  size_t result{1};
  v %= (p - 1);
  for (size_t i{0}; i < v; ++i) {
    result *= base;
    result %= p;
  }
  return result;
}

} // namespace

int main() {
  const auto [r0, r1] {[](){
    std::tuple<size_t, size_t> vals{};
    std::cin >> std::get<0>(vals) >> std::get<1>(vals);
    return vals;
  }()};

  fmt::print("{}\n", baseToV(calcLoopSize(r0) * calcLoopSize(r1)));

  return 0;
}