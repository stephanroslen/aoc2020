#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <vector>

#include <fmt/core.h>

static constexpr int target{2020};

int main() {
  const std::vector<int> data{[]() {
    std::vector<int> result{};
    std::copy(std::istream_iterator<int>{std::cin}, std::istream_iterator<int>{}, std::back_inserter(result));
    return result;
  }()};
  const std::set<int> dataSet{[&data]() {
    std::set<int> result{};
    std::copy(std::cbegin(data), std::cend(data), std::insert_iterator{result, std::begin(result)});
    return result;
  }()};

  // PART 1

  int val00{-1};
  int val01{-1};

  for (const auto& val : data) {
    if (1 == dataSet.count(target - val)) {
      val00 = val;
      val01 = target - val00;
      break;
    }
  }

  fmt::print("{} * {} = {}\n", val00, val01, val00 * val01);

  // PART 2

  int val10{-1};
  int val11{-1};
  int val12{-1};

  for (const auto& val0 : data) {
    for (const auto& val1 : data) {
      if (1 == dataSet.count(target - val0 - val1)) {
        val10 = val0;
        val11 = val1;
        val12 = target - val0 - val1;
        break;
      }
    }
  }

  fmt::print("{} * {} * {} = {}\n", val10, val11, val12, val10 * val11 * val12);

  return 0;
}
