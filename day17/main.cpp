#include <array>
#include <iostream>
#include <unordered_map>
#include <unordered_set>

#include <fmt/core.h>

namespace {

template<size_t N>
struct IntNd {
  std::array<int, N> loc;

  template<typename... Args>
  constexpr IntNd(Args... args) noexcept : loc{args...} {}

  constexpr bool operator==(const IntNd& rhs) const noexcept {
    for (size_t i{0}; i < N; ++i) { // no constexpr operator== :(
      if (loc[i] != rhs.loc[i]) {
        return false;
      }
    }
    return true;
  }
  constexpr bool operator!=(const IntNd& rhs) const noexcept { return !(rhs == *this); }

  constexpr IntNd& operator+=(const IntNd& other) noexcept {
    for (size_t i{0}; i < N; ++i) {
      loc[i] += other.loc[i];
    }
    return *this;
  }

  constexpr IntNd operator+(const IntNd& other) const noexcept {
    IntNd result{*this};
    result += other;
    return result;
  }
};

} // namespace

namespace std {
template<size_t N>
struct hash<IntNd<N>> {
  constexpr size_t operator()(const IntNd<N>& val) const noexcept {
    size_t result{};
    for (size_t i{0}; i < N; ++i) {
      result <<= 12;
      result ^= static_cast<size_t>(val.loc[i]) ^ (result >> 42);
    }
    return result;
  }
};
} // namespace std

namespace {

// alternative ideas: either some hilbert space or as we only simulate 6 timeslots a limited world in a bitset
template<size_t N>
using WorldNd = std::unordered_set<IntNd<N>>;

template<typename T>
struct Offsets;

template<size_t N>
struct Offsets<IntNd<N>> {
  using CoordType = IntNd<N>;

  constexpr static size_t OffsetDim() noexcept {
    size_t result{1};
    for (size_t i{0}; i < N; ++i) {
      result *= 3;
    }
    result -= 1;
    return result;
  }

  constexpr static auto OffsetArray() noexcept {
    constexpr size_t dim{OffsetDim()};
    std::array<CoordType, dim> result{};
    CoordType coord{};
    for (size_t iDim{0}; iDim < N; ++iDim) {
      coord.loc[iDim] = -1; // no constexpr std::fill :(
    }
    constexpr auto nextCoord{[](CoordType in) -> CoordType {
      for (size_t iDim{0}; iDim < N; ++iDim) {
        in.loc[iDim] += 1;
        if (in.loc[iDim] != 2) {
          break;
        }
        in.loc[iDim] = -1;
      }
      return in;
    }};
    for (size_t i{0}; i < dim; ++i) {
      result[i] = coord;
      if (i == dim - 1) {
        break;
      }
      coord = nextCoord(coord);
      if (coord == CoordType{}) {
        coord = nextCoord(coord);
      }
    }
    return result;
  }

  constexpr static auto offsets{OffsetArray()};
};

template<typename WorldType>
WorldType simulate(const WorldType& input) noexcept {
  WorldType result{};
  using CoordType = typename WorldType::value_type;
  constexpr auto offsets{Offsets<CoordType>::offsets};

  std::unordered_map<CoordType, int> activeCnt{};
  for (const auto& coord : input) {
    for (const auto& offset : offsets) {
      activeCnt[coord + offset] += 1;
    }
  }
  for (const auto& [coord, cnt] : activeCnt) {
    if (cnt == 3 || (cnt == 2 && input.contains(coord))) {
      result.emplace(coord);
    }
  }
  return result;
}

} // namespace

int main() {
  auto [activeMap3d, activeMap4d] = []() {
    std::string line{};
    int iy{0};
    std::tuple<WorldNd<3>, WorldNd<4>> result{};
    auto& [result3d, result4d] = result;
    while (std::getline(std::cin, line)) {
      for (int ix{0}; ix < static_cast<int>(line.size()); ++ix) {
        if (line[ix] == '#') {
          result3d.emplace(ix, iy, 0);
          result4d.emplace(ix, iy, 0, 0);
        }
      }
      iy += 1;
    }
    return result;
  }();

  for (int i{0}; i < 6; ++i) {
    activeMap3d = simulate(activeMap3d);
    activeMap4d = simulate(activeMap4d);
  }

  fmt::print("{}\n{}\n", activeMap3d.size(), activeMap4d.size());

  return 0;
}