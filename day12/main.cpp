#include <iostream>

#include <fmt/core.h>

#include <baselib/IntegerCoord.h>

template<typename T>
struct TypeC {};

struct MoveStep {
  enum class Instr { North, West, South, East, Left, Right, Forward };

  Instr instr;
  int number;

  friend std::istream& operator>>(std::istream& is, MoveStep& moveStep) noexcept {
    char c{0};
    is >> c;
    switch (c) {
    case 'N': moveStep.instr = Instr::North; break;
    case 'W': moveStep.instr = Instr::West; break;
    case 'S': moveStep.instr = Instr::South; break;
    case 'E': moveStep.instr = Instr::East; break;
    case 'L': moveStep.instr = Instr::Left; break;
    case 'R': moveStep.instr = Instr::Right; break;
    case 'F': moveStep.instr = Instr::Forward; break;
    default: return is;
    }
    return is >> moveStep.number;
  }
};

struct BaseTurtle {
  baselib::IntCoord coord{0, 0};

  enum class Direction { North = 0, West = 1, South = 2, East = 3 };

  static baselib::IntCoord DecodeDirection(const Direction& direction, int number) noexcept {
    switch (direction) {
    case Direction::North: return {0, number};
    case Direction::West: return {-number, 0};
    case Direction::South: return {0, -number};
    case Direction::East: return {number, 0};
    }
    std::terminate();
  }

  static int DecodeRotation(bool left, int degree) noexcept {
    if (!left) {
      degree = 360 - degree;
      if (degree == 360) {
        degree = 0;
      }
    }
    return degree / 90;
  }

  void Step(const MoveStep& step) noexcept {
    switch (step.instr) {
    case MoveStep::Instr::North: ApplyDirection(Direction::North, step.number); break;
    case MoveStep::Instr::West: ApplyDirection(Direction::West, step.number); break;
    case MoveStep::Instr::South: ApplyDirection(Direction::South, step.number); break;
    case MoveStep::Instr::East: ApplyDirection(Direction::East, step.number); break;
    case MoveStep::Instr::Left: Rotate(DecodeRotation(true, step.number)); break;
    case MoveStep::Instr::Right: Rotate(DecodeRotation(false, step.number)); break;
    case MoveStep::Instr::Forward: Forward(step.number); break;
    }
  };

  virtual void ApplyDirection(const Direction& direction, int number) noexcept = 0;
  virtual void Rotate(int decRot) noexcept                                     = 0;
  virtual void Forward(int number) noexcept                                    = 0;

 protected:
  ~BaseTurtle() = default; // forbid direct use of base-class / sharding
};

struct Turtle final : public BaseTurtle {
  Direction facing{Direction::East};

  void ApplyDirection(const Direction& direction, int number) noexcept final {
    coord += DecodeDirection(direction, number);
  }

  void Rotate(int decRot) noexcept final {
    int tmp = static_cast<std::underlying_type_t<Direction>>(facing);
    tmp += decRot;
    if (tmp >= 4) {
      tmp -= 4;
    }
    facing = static_cast<Direction>(tmp);
  }

  void Forward(int number) noexcept final { ApplyDirection(facing, number); }
};

struct LinAlgTurtle final : public BaseTurtle {
  baselib::IntCoord facing{10, 1};

  void ApplyDirection(const Direction& direction, int number) noexcept final {
    facing += DecodeDirection(direction, number);
  }

  void Rotate(int decRot) noexcept final {
    const auto oldDir = facing;
    if (decRot == 1) {
      facing = {-oldDir.y, oldDir.x};
    } else if (decRot == 2) {
      facing = {-oldDir.x, -oldDir.y};
    } else if (decRot == 3) {
      facing = {oldDir.y, -oldDir.x};
    }
  }

  void Forward(int number) noexcept final { coord += facing * number; }
};

int main() {
  const auto movePath{[]() {
    std::vector<MoveStep> result{};
    std::copy(std::istream_iterator<MoveStep>{std::cin}, std::istream_iterator<MoveStep>{}, std::back_inserter(result));
    return result;
  }()};

  const auto solver{[&]<typename TurtleType>(const TypeC<TurtleType>) {
    TurtleType turtle{};
    for (const MoveStep& moveStep : movePath) {
      turtle.Step(moveStep);
    }
    return turtle.coord.ManDist({0, 0});
  }};

  fmt::print("{}\n", solver(TypeC<Turtle>{}));
  fmt::print("{}\n", solver(TypeC<LinAlgTurtle>{}));

  return 0;
}