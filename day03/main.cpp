#include <algorithm>
#include <array>
#include <iostream>
#include <iterator>
#include <numeric>
#include <set>
#include <vector>

#include <fmt/core.h>

#include <baselib/IntegerCoord.h>

struct Map {
  std::vector<std::string> data;

  size_t Width() const { return data[0].size(); }

  size_t Height() const { return data.size(); }

  bool IsTreeAt(size_t x, size_t y) const {
    x %= Width();
    return '#' == data[y][x];
  }

  bool IsTreeAt(const baselib::IntegerCoord<size_t>& position) const { return IsTreeAt(position.x, position.y); }

  struct SlopeIteratorProxy {
    struct SlopeIterator {
      using difference_type   = size_t;
      using value_type        = baselib::IntegerCoord<size_t>;
      using pointer           = baselib::IntegerCoord<size_t>*;
      using reference         = baselib::IntegerCoord<size_t>&;
      using iterator_category = std::input_iterator_tag;

      baselib::IntegerCoord<size_t> slope;
      baselib::IntegerCoord<size_t> position;

      SlopeIterator& operator++() {
        position += slope;
        return *this;
      }

      bool operator!=(const SlopeIterator& other) const { return position.y != other.position.y; }

      const baselib::IntegerCoord<size_t>& operator*() const { return position; }
    };

    SlopeIterator beginIterator;
    SlopeIterator endIterator;

    SlopeIterator begin() const { return beginIterator; }

    SlopeIterator end() const { return endIterator; }
  };

  SlopeIteratorProxy MakeSlopeGenerator(const baselib::IntegerCoord<size_t>& slope) const {
    return SlopeIteratorProxy{{slope, {0, 0}}, {{0, 0}, {0, (Height() + slope.y - 1) / slope.y * slope.y}}};
  }
};

size_t CountTrees(const Map& map, const baselib::IntegerCoord<size_t>& slope) {
  const auto slopeGen = map.MakeSlopeGenerator(slope);
  return std::count_if(std::cbegin(slopeGen), std::cend(slopeGen), [&map](const baselib::IntegerCoord<size_t>& position) {
    return map.IsTreeAt(position);
  });
}

int main() {
  const Map map{[]() {
    Map result{};
    while (std::cin) {
      std::string buffer;
      std::cin >> buffer;
      result.data.emplace_back(buffer);
    }
    return result;
  }()};

  // PART 1
  fmt::print("{}\n", CountTrees(map, {3, 1}));
  // PART 2
  const std::array<baselib::IntegerCoord<size_t>, 5> slopes{{{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}}};
  fmt::print("{}\n",
             std::transform_reduce(std::cbegin(slopes),
                                   std::cend(slopes),
                                   size_t{1},
                                   std::multiplies<>{},
                                   [&map](const baselib::IntegerCoord<size_t>& slope) { return CountTrees(map, slope); }));
  return 0;
}
