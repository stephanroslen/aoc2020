#include <iostream>
#include <numeric>
#include <optional>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

#include <fmt/core.h>

namespace {

struct Entry {
  std::optional<ssize_t> value;

  friend std::istream& operator>>(std::istream& is, Entry& entry) {
    std::string tmp{};
    if (is >> tmp) {
      if (tmp == "x") {
        entry.value = std::nullopt;
      } else {
        entry.value = std::stoi(tmp);
      }
    }
    return is;
  }
};

ssize_t inverse(ssize_t a, ssize_t n) {
  ssize_t t{0};
  ssize_t tNew{1};
  ssize_t r{n};
  ssize_t rNew{a};

  while (0 != rNew) {
    const auto quot   = r / rNew;
    std::tie(t, tNew) = std::tuple<ssize_t, ssize_t>{tNew, t - quot * tNew};
    std::tie(r, rNew) = std::tuple<ssize_t, ssize_t>{rNew, r - quot * rNew};
  }
  if (t < 0) {
    t += n;
  }
  if (t > n) {
    t -= n;
  }
  return t;
}

ssize_t CRT(const std::vector<std::tuple<ssize_t, ssize_t>>& congruences) {
  const ssize_t N{std::transform_reduce(std::cbegin(congruences),
                                        std::cend(congruences),
                                        ssize_t{1},
                                        std::multiplies{},
                                        [](const auto& cs) { return std::get<1>(cs); })};
  return std::transform_reduce(std::cbegin(congruences),
                               std::cend(congruences),
                               ssize_t{0},
                               std::plus{},
                               [&](const auto& cs) {
                                 const auto& [congruence, prime] = cs;
                                 const ssize_t yi                = N / prime;
                                 const ssize_t zi                = inverse(yi, prime);
                                 return congruence * yi * zi;
                               })
         % N;
}

} // namespace

int main() {
  const auto minuteStart{[]() {
    ssize_t result;
    std::cin >> result;
    return result;
  }()};
  const std::string entryLine{[]() {
    std::string result;
    std::cin >> result;
    std::transform(std::begin(result), std::end(result), std::begin(result), [](char v) {
      if (v == ',') {
        v = ' ';
      }
      return v;
    });
    return result;
  }()};

  const auto entries{[&]() {
    std::vector<Entry> result{};
    std::stringstream entryStream{entryLine};
    std::copy(std::istream_iterator<Entry>{entryStream}, std::istream_iterator<Entry>{}, std::back_inserter(result));
    return result;
  }()};

  const auto part1{[&]() {
    for (ssize_t iMinute{minuteStart};; ++iMinute) {
      for (const auto& entry : entries) {
        if (!entry.value) {
          continue;
        }
        const auto value = *entry.value;
        if (iMinute % value == 0) {
          return (iMinute - minuteStart) * value;
        }
      }
    }
  }()};
  fmt::print("{}\n", part1);

  const auto part2{[&]() {
    std::vector<std::tuple<ssize_t, ssize_t>> CRTCalc{};
    for (ssize_t i{0}; i < static_cast<ssize_t>(entries.size()); ++i) {
      if (entries[i].value) {
        const ssize_t prime      = *entries[i].value;
        const ssize_t congruence = prime - i;
        CRTCalc.emplace_back(congruence, prime);
      }
    }
    return CRT(CRTCalc);
  }()};

  fmt::print("{}\n", part2);

  return 0;
}
