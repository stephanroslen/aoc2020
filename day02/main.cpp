#include <algorithm>
#include <iostream>
#include <iterator>
#include <set>
#include <vector>

#include <fmt/core.h>
#include <fmt/ostream.h>

struct Entry {
  int num0;
  int num1;
  char item;
  std::string password;

  std::istream& ReadFrom(std::istream& is) {
    is >> num0;
    is.ignore();
    is >> num1;
    is.ignore();
    is >> item;
    is.ignore(2);
    is >> password;
    return is;
  }

  std::ostream& WriteTo(std::ostream& os) const {
    fmt::print(os, "{} {} {} {}", num0, num1, item, password);
    return os;
  }

  friend std::istream& operator>>(std::istream& is, Entry& entry) { return entry.ReadFrom(is); }

  friend std::ostream& operator<<(std::ostream& os, const Entry& entry) { return entry.WriteTo(os); }

  bool isValid1() const {
    auto itemCnt = std::count(std::cbegin(password), std::cend(password), item);
    return (itemCnt >= num0) && (itemCnt <= num1);
  }

  bool isValid2() const { return (password[num0 - 1] == item) != (password[num1 - 1] == item); }
};

int main() {
  const auto entries{[]() {
    std::vector<Entry> result;
    std::copy(std::istream_iterator<Entry>(std::cin), std::istream_iterator<Entry>(), std::back_inserter(result));
    return result;
  }()};
  // PART 1
  fmt::print("{}\n", std::count_if(std::cbegin(entries), std::cend(entries), [](const Entry& entry) {
               return entry.isValid1();
             }));
  // PART 2
  fmt::print("{}\n", std::count_if(std::cbegin(entries), std::cend(entries), [](const Entry& entry) {
               return entry.isValid2();
             }));
  return 0;
}
