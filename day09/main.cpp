#include <iostream>
#include <iterator>
#include <vector>

#include <fmt/core.h>

int main() {
  const std::vector<int> input{[]() {
    std::vector<int> result{};
    result.reserve(1000);
    std::copy(std::istream_iterator<int>{std::cin}, std::istream_iterator<int>{}, std::back_inserter(result));
    return result;
  }()};
  const int part1{[&]() {
    int result{};
    auto iter{std::cbegin(input)};
    auto backIter{iter};
    std::advance(iter, 25);
    while (true) {
      const int value{*iter};
      for (auto rangeIter0{backIter}; std::next(rangeIter0) != iter; ++rangeIter0) {
        const int bVal0{*rangeIter0};
        for (auto rangeIter1{std::next(rangeIter0)}; rangeIter1 != iter; ++rangeIter1) {
          const int bVal1{*rangeIter1};
          if (bVal0 + bVal1 == value) {
            goto out_found;
          }
        }
      }
      result = value;
      break;
    out_found:
      ++iter;
      ++backIter;
    };
    return result;
  }()};

  fmt::print("{}\n", part1);

  const int part2{[&]() {
    auto iterStart = std::cbegin(input);
    auto iterFin{std::next(iterStart)};
    int sum{*iterStart + *iterFin};
    while (true) {
      if (sum < part1) {
        ++iterFin;
        sum += *iterFin;
      } else if (sum > part1) {
        sum -= *iterStart;
        ++iterStart;
      } else {
        ++iterFin;
        break;
      }
    }

    const auto [minEl, maxEl] = std::minmax_element(iterStart, iterFin);
    return *minEl + *maxEl;
  }()};

  fmt::print("{}\n", part2);

  return 0;
}