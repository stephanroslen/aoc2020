#include <iostream>
#include <memory_resource>
#include <string>
#include <unordered_set>
#include <vector>

#include <fmt/core.h>

struct GameState {
  std::array<std::vector<int>, 2> decks{};

  bool operator==(const GameState& rhs) const { return decks == rhs.decks; }
  bool operator!=(const GameState& rhs) const { return !(rhs == *this); }

  size_t Hash() const {
    size_t result{decks[0].size() << 32 || decks[1].size()};
    const auto hashEntry{[&](const int v) {
      result = (result << 10) ^ (result >> 49);
      result += v;
    }};
    std::for_each(std::cbegin(decks[0]), std::cend(decks[0]), hashEntry);
    std::for_each(std::cbegin(decks[1]), std::cend(decks[1]), hashEntry);
    return result;
  }

  std::istream& ReadFrom(std::istream& is) {
    decks[0].clear();
    decks[1].clear();

    int readState{0};
    std::string line{};

    while (std::getline(is, line)) {
      if (line.empty() || line.starts_with("Player")) {
        readState += 1;
        continue;
      }
      int val{std::stoi(line)};
      decks[readState == 1 ? 0 : 1].emplace_back(val);
    }

    return is;
  }

  int RoundWinner() const { return (decks[0].front() > decks[1].front()) ? 0 : 1; }

  static int OtherPlayer(int player) { return player == 1 ? 0 : 1; }

  void RotateBack() {
    std::rotate(std::begin(decks[0]), std::next(std::begin(decks[0])), std::end(decks[0]));
    std::rotate(std::begin(decks[1]), std::next(std::begin(decks[1])), std::end(decks[1]));
  }

  template<typename WinDetMorph>
  void Step(WinDetMorph&& winDetMorph) {
    int winner{winDetMorph(*this)};
    int nonWinner{OtherPlayer(winner)};

    RotateBack();
    decks[winner].emplace_back(decks[nonWinner].back());
    decks[nonWinner].pop_back();
  }

  bool Finished() const { return decks[0].size() == 0 || decks[1].size() == 0; }

  int Score(int player) const {
    int result{0};
    const auto& deck = decks[player];
    int cardVal      = deck.size();
    for (const auto& card : deck) {
      result += card * cardVal;
      cardVal -= 1;
    }
    return result;
  }

  int Winner() const { return ((decks[0].size() != 0) ? 0 : 1); }

  int Score() const { return Score(Winner()); }
};

namespace std {
template<>
struct hash<GameState> {
  size_t operator()(const GameState& gameState) const { return gameState.Hash(); }
};
} // namespace std

std::tuple<int, int> simulate1(GameState&& gameState) {
  while (!gameState.Finished()) {
    gameState.Step([&](const GameState& gs) { return gs.RoundWinner(); });
  }
  return {gameState.Score(), gameState.RoundWinner()};
}

std::tuple<int, int> simulate2(GameState&& gameState) {
  std::pmr::monotonic_buffer_resource resource{};
  std::pmr::unordered_set<GameState> knownStates{&resource};
  knownStates.reserve(1024); // can grow much further, but this seems to be a fair choice for overall performance
  while (!gameState.Finished()) {
    gameState.Step([&](const GameState& gs) {
      const int draw0 = gs.decks[0].front();
      const int draw1 = gs.decks[1].front();
      if ((draw0 < static_cast<int>(gs.decks[0].size())) && (draw1 < static_cast<int>(gs.decks[1].size()))) {
        GameState subGame{gs};
        subGame.RotateBack();
        subGame.decks[0].resize(draw0);
        subGame.decks[1].resize(draw1);
        auto [sgS, sgW] = simulate2(std::move(subGame));
        return sgW;
      } else {
        return gs.RoundWinner();
      }
    });
    if (knownStates.contains(gameState)) {
      return {gameState.Score(0), 0};
    } else {
      knownStates.emplace(gameState);
    }
  }
  return {gameState.Score(), gameState.Winner()};
}

int main() {
  GameState gameState{};
  gameState.ReadFrom(std::cin);

  fmt::print("{}\n", std::get<0>(simulate1(GameState{gameState})));
  fmt::print("{}\n", std::get<0>(simulate2(GameState{gameState})));

  return 0;
}