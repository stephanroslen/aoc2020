#include <iostream>
#include <stack>
#include <string>
#include <string_view>

#include <fmt/core.h>

namespace {

enum Ops { Par, Plus, Mult };

struct Stacks {
  std::stack<Ops> opStack{};
  std::stack<size_t> valStack{};

  void Work() noexcept {
    const Ops topOp{opStack.top()};
    opStack.pop();
    const size_t v0{valStack.top()};
    valStack.pop();
    const size_t v1{valStack.top()};
    valStack.pop();
    if (Mult == topOp) {
      valStack.push(v0 * v1);
    } else {
      valStack.push(v0 + v1);
    }
  }

  template<bool ApplyPrecedence>
  size_t Eval(std::string_view data) noexcept {
    while (0 != data.size()) {
      const char element = data[0];
      data               = data.substr(data[1] == ' ' ? 2 : 1);
      if ('(' == element) {
        opStack.push(Par);
      } else if (')' == element) {
        while (Par != opStack.top()) {
          Work();
        }
        opStack.pop();
      } else if ('+' == element || '*' == element) {
        const Ops op{'+' == element ? Plus : Mult};
        while (!opStack.empty() && (opStack.top() != Par)
               && (!ApplyPrecedence || Mult == op || (Plus == op && Plus == opStack.top())))
        {
          Work();
        }
        opStack.push(op);
      } else {
        valStack.push(element - '0' + 0);
      }
    }
    while (!opStack.empty()) {
      Work();
    }
    const size_t result{valStack.top()};
    valStack.pop();
    return result;
  }
};

} // namespace

int main() {
  std::string line{};
  size_t part1{0}, part2{0};
  Stacks stacks{};
  while (std::getline(std::cin, line)) {
    part1 += stacks.Eval<false>(line);
    part2 += stacks.Eval<true>(line);
  }
  fmt::print("{}\n", part1);
  fmt::print("{}\n", part2);
  return 0;
}