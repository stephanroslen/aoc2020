#include <array>
#include <iostream>

#include <fmt/core.h>

#include <baselib/IntegerCoord.h>

struct World {
  enum State { None, Empty, Taken };
  std::vector<std::vector<State>> map;

  baselib::IntCoord size;

  bool operator==(const World& other) const { return map == other.map; }

  void ReadFrom(std::istream& is) {
    std::string line{};
    is >> line;
    size.x = line.size();
    const auto parseline{[&]() {
      std::vector<State> row(size.x, State::None);
      for (int i{0}; i < size.x; ++i) {
        State& s{row[i]};
        char c{line[i]};
        if (c == 'L') {
          s = State::Empty;
        } else if (c == '#') {
          s = State::Taken;
        }
      }
      return row;
    }};
    map.emplace_back(parseline());
    while (is >> line) {
      map.emplace_back(parseline());
    }
    size.y = map.size();
  }

  State& StateAt(const baselib::IntCoord& coord) { return map[coord.y][coord.x]; }

  const State& StateAt(const baselib::IntCoord& coord) const { return map[coord.y][coord.x]; }

  template<typename Morphism>
  void EachCoord(Morphism&& morphism) const {
    for (int ix{0}; ix < size.x; ++ix) {
      for (int iy{0}; iy < size.y; ++iy) {
        morphism(baselib::IntCoord{ix, iy});
      }
    }
  }

  bool IsValidCoord(const baselib::IntCoord& candidate) const {
    return (candidate.x >= 0 && candidate.x < size.x && candidate.y >= 0 && candidate.y < size.y);
  }

  template<typename Morphism>
  bool TryCoord(const baselib::IntCoord& candidate, Morphism&& morphism) const {
    if (IsValidCoord(candidate)) {
      morphism(candidate);
      return StateAt(candidate) == State::None;
    }
    return false;
  }

  template<bool StopAfterOne, typename Morphism>
  void EachVisibleCoord(const baselib::IntCoord& coord, Morphism&& morphism) const {
    constexpr std::array<baselib::IntCoord, 8> offsets{{{-1, -1}, {0, -1}, {1, -1}, {-1, 0}, {1, 0}, {-1, 1}, {0, 1}, {1, 1}}};
    for (const auto& offset : offsets) {
      auto tmpCoord{coord + offset};
      while (TryCoord(tmpCoord, morphism)) {
        if constexpr (StopAfterOne) {
          break;
        }
        tmpCoord += offset;
      }
    }
  }

  template<bool StopAfterOne, int NeighbourThreshold>
  void Simulate(World& newWorld) {
    newWorld = *this;
    EachCoord([&](const baselib::IntCoord& coord) {
      const auto state = StateAt(coord);
      if (state == State::None) {
        return;
      }
      int countedNeighbours{0};
      EachVisibleCoord<StopAfterOne>(coord, [&](const baselib::IntCoord& adjCoord) {
        if (StateAt(adjCoord) == State::Taken) {
          countedNeighbours += 1;
        }
      });
      if (state == State::Empty && countedNeighbours == 0) {
        newWorld.StateAt(coord) = State::Taken;
      } else if (state == State::Taken && countedNeighbours >= NeighbourThreshold) {
        newWorld.StateAt(coord) = State::Empty;
      }
    });
  }
};

int main() {
  const auto world{[]() {
    World result{};
    result.ReadFrom(std::cin);
    return result;
  }()};

  const auto solver{[]<typename StopAfterOneT, typename NeighbourThresholdT>(const World& worldIn,
                                                                             const StopAfterOneT,
                                                                             const NeighbourThresholdT) {
    size_t result{0};
    std::array<World, 2> worlds{worldIn, {}};
    World* oldWorld = &worlds[0];
    World* newWorld = &worlds[1];
    for (;;) {
      oldWorld->Simulate<StopAfterOneT::value, NeighbourThresholdT::value>(*newWorld);
      if (*oldWorld == *newWorld) {
        break;
      }
      std::swap(oldWorld, newWorld);
    }
    newWorld->EachCoord([&](const baselib::IntCoord& coord) {
      if (newWorld->StateAt(coord) == World::State::Taken) {
        result += 1;
      }
    });
    return result;
  }};

  fmt::print("{}\n", solver(world, std::integral_constant<bool, true>{}, std::integral_constant<int, 4>{}));
  fmt::print("{}\n", solver(world, std::integral_constant<bool, false>{}, std::integral_constant<int, 5>{}));

  return 0;
}