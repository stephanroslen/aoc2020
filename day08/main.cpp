#include <iostream>
#include <string>
#include <string_view>
#include <vector>

#include <fmt/core.h>

struct CPU {
  int acc{0};
  int iptr{0};

  enum Op { Acc, Jmp, Nop };

  enum HaltCondition { Loop, End };

  struct Instruction {
    Op op;
    int arg;

    friend std::istream& operator>>(std::istream& is, Instruction& insn) noexcept {
      std::string buffer{};
      is >> buffer;
      const auto assignAndRead{[&](const Op val) {
        insn.op = val;
        is >> insn.arg;
      }};
      const std::string_view svBuffer{buffer};
      if (svBuffer == "acc") {
        assignAndRead(Acc);
      } else if (svBuffer == "jmp") {
        assignAndRead(Jmp);
      } else if (svBuffer == "nop") {
        assignAndRead(Nop);
      }
      return is;
    }
  };

  std::vector<Instruction> code{};

  void Step() noexcept { Execute(code[iptr]); }

  void Execute(Instruction insn) noexcept { Execute(insn.op, insn.arg); }

  void Execute(Op op, int arg) noexcept {
    switch (op) {
    case Acc: acc += arg; break;
    case Jmp: iptr += arg; break;
    case Nop: break;
    }
    if (op != Jmp) {
      iptr += 1;
    }
  }

  void Reset() noexcept {
    iptr = 0;
    acc  = 0;
  }

  bool Ended() const noexcept { return iptr >= static_cast<int>(code.size()); }

  template<typename Morphism>
  HaltCondition Run(Morphism&& morphism) noexcept(noexcept(morphism(*this))) {
    std::vector<bool> visitedInsns(code.size(), false);
    while (morphism(*this) && !Ended() && !visitedInsns.at(iptr)) {
      visitedInsns.at(iptr) = true;
      Step();
    }
    return Ended() ? HaltCondition::End : HaltCondition::Loop;
  }

  HaltCondition Run() noexcept {
    return Run([](const auto&) { return true; });
  }
};

int main() {
  CPU cpu{};
  std::copy(std::istream_iterator<CPU::Instruction>(std::cin),
            std::istream_iterator<CPU::Instruction>(),
            std::back_inserter(cpu.code));

  auto cpu1{cpu};
  cpu1.Run();
  fmt::print("{}\n", cpu1.acc);

  auto metaCpu{cpu};
  metaCpu.Run([&cpu](const CPU& runningCpu) {
    const auto i = runningCpu.iptr;
    if (cpu.code[i].op == CPU::Op::Acc) {
      return true;
    }
    auto cpu2{runningCpu};
    auto& op = cpu2.code[i].op;
    if (op == CPU::Op::Jmp) {
      op = CPU::Op::Nop;
    } else if (op == CPU::Op::Nop) {
      op = CPU::Op::Jmp;
    }
    const auto haltCond = cpu2.Run();
    if (haltCond == CPU::HaltCondition::End) {
      fmt::print("{}\n", cpu2.acc);
      return false;
    }
    return true;
  });
}