#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

#include <fmt/core.h>

#include <baselib/Algorithm.h>

template<typename InnerIter>
struct AdjacentDiffGenerator {
  AdjacentDiffGenerator(InnerIter begin, InnerIter end)
      : beginIterator{begin, (begin != end) ? std::next(begin) : begin}, endIterator{end, end} {};
  struct AdjacentDiffIter {
    InnerIter current;
    InnerIter currentNext;

    using difference_type   = typename InnerIter::difference_type;
    using value_type        = typename InnerIter::value_type;
    using pointer           = typename InnerIter::pointer;
    using reference         = typename InnerIter::reference;
    using iterator_category = std::input_iterator_tag;

    AdjacentDiffIter& operator++() {
      ++current;
      ++currentNext;
      return *this;
    }

    AdjacentDiffIter operator++(int) {
      AdjacentDiffIter tmp{*this};
      operator++();
      return tmp;
    }

    bool operator!=(const AdjacentDiffIter& other) const {
      return current != other.current && currentNext != other.currentNext;
    }

    int operator*() const { return *currentNext - *current; }
  };

  AdjacentDiffIter beginIterator;
  AdjacentDiffIter endIterator;

  auto begin() const { return beginIterator; }

  auto end() const { return endIterator; }
};

int main() {
  const auto adapters{[]() {
    std::vector<int> result{0};
    std::copy(std::istream_iterator<int>{std::cin}, std::istream_iterator<int>{}, std::back_inserter(result));
    std::sort(std::begin(result), std::end(result));
    result.push_back(result.back() + 3);
    return result;
  }()};

  const auto part1{[&]() {
    const AdjacentDiffGenerator adjDiffGen(std::cbegin(adapters), std::cend(adapters));
    const auto classification{baselib::CountClassification<3>(std::cbegin(adjDiffGen),
                                                              std::cend(adjDiffGen),
                                                              [](const int val) { return val - 1; })};
    return classification[0] * classification[2];
  }()};

  fmt::print("{}\n", part1);

  const auto part2{[&]() {
    std::vector<size_t> paths(adapters.size(), 0);
    size_t idx{adapters.size() - 1};
    paths[idx] = 1;
    while (idx > 0) {
      idx -= 1;
      for (size_t subIdx{idx + 1}; subIdx < adapters.size(); ++subIdx) {
        if (adapters[idx] + 3 >= adapters[subIdx]) {
          paths[idx] += paths[subIdx];
        } else {
          break;
        }
      }
    }
    return paths[0];
  }()};

  fmt::print("{}\n", part2);

  return 0;
}