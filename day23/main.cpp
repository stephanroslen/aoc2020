#include <iostream>
#include <iterator>
#include <list>
#include <memory_resource>
#include <string>

#include <fmt/core.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>

struct State {
  std::pmr::list<int> cups{};
  std::pmr::vector<std::pmr::list<int>::const_iterator> iterCache{};
  int min{};
  int max{};
  int curr{};

  void PrepareCache() {
    iterCache.resize(max + 1);
    auto iter{cups.cbegin()};
    while (iter != cups.cend()) {
      iterCache.at(*iter) = iter;
      ++iter;
    }
  }

  std::istream& ReadFrom(std::istream& is) {
    std::string line{};
    if (std::getline(is, line)) {
      cups.clear();
      iterCache.clear();
      curr = 0;
      min  = std::numeric_limits<int>::max();
      max  = 0;
      for (const auto c : line) {
        const int val = c - '0' + 0;
        cups.emplace_back(val);
        min = std::min(min, val);
        max = std::max(max, val);
      }
    }
    return is;
  }

  void FillUp() {
    for (size_t i{cups.size()}; i < 1000000; ++i) {
      cups.emplace_back(++max);
    }
  }

  void Step() {
    while (0 != curr) {
      cups.splice(std::cend(cups), cups, std::cbegin(cups));
      curr -= 1;
    }

    const auto wrap{[&](int val) { return (val >= min) ? val : (val - min + max + 1); }};
    const int vFind{[&]() {
      int result{*std::cbegin(cups)};
      auto subRangeB{std::next(std::cbegin(cups))};
      auto subRangeE{subRangeB};
      std::advance(subRangeE, 3);
      while (std::find(subRangeB, subRangeE, result = wrap(result - 1)) != subRangeE) {
      }
      return result;
    }()};

    const auto targetIter{std::next(iterCache.at(vFind))};

    for (int i{0}; i < 3; ++i) {
      cups.splice(targetIter, cups, std::next(std::cbegin(cups)));
    }

    curr += 1;
  }

  void Out1(std::ostream& os) const {
    const auto cupsOut{[&]() {
      auto result{cups};
      auto newBegin = std::next(std::find(std::begin(result), std::end(result), 1));
      if (newBegin == std::end(result)) {
        newBegin = std::begin(result);
      }
      std::rotate(std::begin(result), newBegin, std::end(result));
      result.pop_back();
      return result;
    }()};
    fmt::print(os, "{}\n", fmt::join(cupsOut, ""));
  }

  void Out2(std::ostream& os) const {
    long int result{1};
    auto iter{iterCache.at(1)};
    const auto calcStep{[&]() {
      std::advance(iter, 1);
      if (iter == std::cend(cups)) {
        iter = std::cbegin(cups);
      }
      result *= *iter;
    }};
    calcStep();
    calcStep();
    fmt::print(os, "{}\n", result);
  }
};

int main() {
  std::pmr::monotonic_buffer_resource resource{};
  std::pmr::set_default_resource(&resource);

  const auto state{[]() {
    State result{};
    result.ReadFrom(std::cin);
    return result;
  }()};

  {
    State state1{state};
    state1.PrepareCache();
    for (size_t i{0}; i < 100; ++i) {
      state1.Step();
    }
    state1.Out1(std::cout);
  }

  {
    State state2{state};
    state2.FillUp();
    state2.PrepareCache();
    for (size_t i{0}; i < 10000000; ++i) {
      state2.Step();
    }
    state2.Out2(std::cout);
  }

  return 0;
}