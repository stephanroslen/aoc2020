#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <string>
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <fmt/core.h>
#include <fmt/ranges.h>

struct Food {
  std::vector<std::string> ingredients;
  std::vector<std::string> allergens;

  std::istream& ReadFrom(std::istream& is) {
    std::string line;
    ingredients.clear();
    allergens.clear();
    if (std::getline(is, line)) {
      std::string_view lineSV{line};
      size_t locPar{lineSV.find('(')};
      std::string_view lineIngsSV{lineSV.substr(0, locPar)};
      while (!lineIngsSV.empty()) {
        size_t spacePar{lineIngsSV.find(' ')};
        ingredients.emplace_back(std::string{lineIngsSV.substr(0, spacePar)});
        lineIngsSV.remove_prefix(spacePar + 1);
      }
      std::string_view lineAlleSV{lineSV.substr(locPar + 10, lineSV.size() - locPar - 11)};
      while (!lineAlleSV.empty()) {
        size_t commaPar{lineAlleSV.find(',')};
        if (commaPar == std::string_view::npos) {
          allergens.emplace_back(std::string{lineAlleSV});
          lineAlleSV.remove_prefix(lineAlleSV.size());
        } else {
          allergens.emplace_back(std::string{lineAlleSV.substr(0, commaPar)});
          lineAlleSV.remove_prefix(commaPar + 2);
        }
      }
    }
    return is;
  }

  friend std::istream& operator>>(std::istream& is, Food& food) { return food.ReadFrom(is); }
};

using IngredientSet      = std::unordered_set<std::string>;
using AllergenMap        = std::unordered_map<std::string, IngredientSet>;
using OrderedAllergenMap = std::map<std::string, std::string>;

int main() {
  std::vector<Food> foods{};
  std::copy(std::istream_iterator<Food>{std::cin}, std::istream_iterator<Food>{}, std::back_inserter(foods));
  AllergenMap allergenMap{};

  IngredientSet allIngredients{};
  for (const Food& food : foods) {
    for (const std::string& ing : food.ingredients) {
      allIngredients.emplace(ing);
    }
    for (const std::string& allergen : food.allergens) {
      auto iter = allergenMap.find(allergen);
      if (iter == allergenMap.end()) {
        allergenMap.emplace(allergen, IngredientSet{std::cbegin(food.ingredients), std::cend(food.ingredients)});
      } else {
        auto& [allergen_, possibleIngredients] = *iter;
        IngredientSet curr{std::cbegin(food.ingredients), std::cend(food.ingredients)};
        auto eraseIter{std::begin(possibleIngredients)};
        while (eraseIter != std::cend(possibleIngredients)) {
          if (curr.contains(*eraseIter)) {
            ++eraseIter;
          } else {
            eraseIter = possibleIngredients.erase(eraseIter);
          }
        }
      }
    }
  }

  IngredientSet clearIngredients{allIngredients};
  for (const auto& [allergen, possibleIngredients] : allergenMap) {
    for (const auto& ingredient : possibleIngredients) {
      clearIngredients.erase(ingredient);
    }
  }

  size_t part1{0};

  for (const Food& food : foods) {
    for (const std::string& ingredient : food.ingredients) {
      if (clearIngredients.count(ingredient) != 0) {
        part1 += 1;
      }
    }
  }

  fmt::print("{}\n", part1);

  OrderedAllergenMap orderedAllergenMap{};
  while (!allergenMap.empty()) {
    auto iter{std::begin(allergenMap)};
    while (std::end(allergenMap) != iter) {
      if (iter->second.size() == 1) {
        auto allergen   = iter->first;
        auto ingredient = *std::begin(iter->second);
        orderedAllergenMap.emplace(allergen, ingredient);
        iter = allergenMap.erase(iter);
        for (auto& [allergen_, possibleIngredients] : allergenMap) {
          possibleIngredients.erase(ingredient);
        }
      } else {
        ++iter;
      }
    }
  }

  std::vector<std::string> part2{};
  std::transform(std::cbegin(orderedAllergenMap),
                 std::cend(orderedAllergenMap),
                 std::back_inserter(part2),
                 [](const auto& p) { return p.second; });

  fmt::print("{}\n", fmt::join(part2, ","));

  return 0;
}