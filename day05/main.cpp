#include <iostream>
#include <queue>

#include <fmt/core.h>

int main() {
  auto seats{[]() {
    std::priority_queue<int> result;
    while (std::cin) {
      std::string rs;
      std::getline(std::cin, rs);
      int id{0};
      for (int i{0}; i < 10; ++i) {
        decltype(auto) elem = rs[i];
        id |= (elem == 'B') || (elem == 'R');
        if (i != 9) {
          id <<= 1;
        }
      }

      result.emplace(id);
    }
    return result;
  }()};

  // PART 1

  const auto res1 = seats.top();
  seats.pop();
  fmt::print("{}\n", res1);

  // PART 2

  auto res2 = res1;
  auto tmp{res1};
  do {
    res2 = tmp;
    tmp  = seats.top();
    seats.pop();
  } while (tmp == res2 - 1);
  res2 -= 1;
  fmt::print("{}\n", res2);

  return 0;
}
