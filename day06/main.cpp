#include <bitset>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include <fmt/core.h>

using Row   = std::bitset<26>;
using Entry = std::vector<Row>;

int main() {
  const auto entries{[]() {
    std::vector<Entry> result{};
    Entry entry{};
    while (std::cin) {
      std::string buffer{};
      std::getline(std::cin, buffer);

      if (!buffer.empty()) {
        Row tmp{};
        for (const auto& c : buffer) {
          tmp.set(c - 'a');
        }
        entry.emplace_back(tmp);
      } else {
        result.emplace_back(std::move(entry));
        entry.clear();
      }
    }
    return result;
  }()};

  const auto blockEval{[](auto cBegin, auto cEnd, const Row& init, auto&& morphism) {
    return std::transform_reduce(cBegin, cEnd, size_t{0}, std::plus<>{}, [&](const Entry& e) {
      return std::reduce(std::cbegin(e), std::cend(e), init, morphism).count();
    });
  }};

  // PART 1

  fmt::print("{}\n", blockEval(std::cbegin(entries), std::cend(entries), Row{0}, std::bit_or<>{}));

  // PART 2

  fmt::print("{}\n", blockEval(std::cbegin(entries), std::cend(entries), Row{(1ul << 26) - 1}, std::bit_and<>{}));

  return 0;
}