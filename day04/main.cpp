#include <array>
#include <cctype>
#include <charconv>
#include <iostream>
#include <regex>
#include <unordered_map>
#include <vector>

#include <fmt/core.h>

#include <baselib/Algorithm.h>

struct Passport {
  static bool VerifyByr(std::string_view value) {
    int val{};
    std::from_chars(&value.front(), &value.back() + 1, val);
    return !(val < 1920 || val > 2002);
  }

  static bool VerifyEcl(std::string_view value) {
    static constexpr std::array<std::string_view, 7> validEcls{{"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}};
    return std::find(std::cbegin(validEcls), std::cend(validEcls), value) != std::cend(validEcls);
  }

  static bool VerifyEyr(std::string_view value) {
    int val{};
    std::from_chars(&value.front(), &value.back() + 1, val);
    return !(val < 2020 || val > 2030);
  }

  static bool VerifyHcl(std::string_view value) {
    if (value.size() != 7) {
      return false;
    }
    if (value.at(0) != '#') {
      return false;
    } else {
      int hcl;
      auto hclRes = std::from_chars(&value.front() + 1, &value.back() + 1, hcl, 16);
      if (std::errc::invalid_argument == hclRes.ec) {
        return false;
      };
    }
    return true;
  }

  static bool VerifyHgt(std::string_view value) {
    int val{};
    auto hgtRes = std::from_chars(&value.front(), &value.back() - 1, val);
    if (std::errc::invalid_argument == hgtRes.ec) {
      return false;
    } else {
      if (value.ends_with("cm")) {
        if (val < 150 || val > 193) {
          return false;
        }
      } else if (value.ends_with("in")) {
        if (val < 59 || val > 76) {
          return false;
        }
      } else {
        return false;
      }
    }
    return true;
  }

  static bool VerifyIyr(std::string_view value) {
    int val{};
    std::from_chars(&value.front(), &value.back() + 1, val);
    return !(val < 2010 || val > 2020);
  }

  static bool VerifyPid(std::string_view value) {
    if (value.size() != 9) {
      return false;
    }
    return std::all_of(std::cbegin(value), std::cend(value), [](const char& c) -> bool { return std::isdigit(c); });
  }

  using MetaEntry = std::tuple<std::string_view, bool (*)(std::string_view)>;
  constexpr static std::array<MetaEntry, 7> metaData{{{"byr", VerifyByr},
                                                      {"ecl", VerifyEcl},
                                                      {"eyr", VerifyEyr},
                                                      {"hcl", VerifyHcl},
                                                      {"hgt", VerifyHgt},
                                                      {"iyr", VerifyIyr},
                                                      {"pid", VerifyPid}}};

  std::string raw;

  std::istream& ReadFrom(std::istream& is) {
    std::string entry{};
    std::string buffer{};
    while (is) {
      std::getline(is, buffer);

      if (buffer.empty()) {
        break;
      } else {
        if (!entry.empty()) {
          entry += " ";
        }
        entry += std::move(buffer);
      }
    }
    raw = std::move(entry);

    return is;
  }

  friend std::istream& operator>>(std::istream& is, Passport& passport) { return passport.ReadFrom(is); }

  size_t Classify() const {
    const auto values{[this]() {
      std::array<std::optional<std::string_view>, std::tuple_size_v<decltype(metaData)>> result{};
      std::fill(std::begin(result), std::end(result), std::nullopt);

      std::string_view rawSV{raw};
      while (0 != rawSV.size()) {
        size_t idxEndFirst = rawSV.find_first_of(' ');
        size_t amountConsume{};
        if (std::string_view::npos == idxEndFirst) {
          idxEndFirst   = rawSV.size();
          amountConsume = idxEndFirst;
        } else {
          amountConsume = idxEndFirst + 1;
        }
        const auto firstItem = rawSV.substr(0, idxEndFirst);
        rawSV.remove_prefix(amountConsume);

        const auto idxColon = firstItem.find_first_of(':');
        const auto key      = firstItem.substr(0, idxColon);
        const auto value    = firstItem.substr(idxColon + 1);

        const auto iter = std::find_if(std::cbegin(metaData), std::cend(metaData), [&key](const MetaEntry& metaEntry) {
          return std::get<0>(metaEntry) == key;
        });
        if (iter != std::cend(metaData)) {
          size_t idx     = std::distance(std::cbegin(metaData), iter);
          result.at(idx) = value;
        }
      }
      return result;
    }()};

    bool allValid{true};
    for (size_t i{0}; i < std::tuple_size_v<decltype(values)>; ++i) {
      if (!values.at(i)) {
        return 0;
      }
      if (allValid && !std::get<1>(metaData.at(i))(*values.at(i))) {
        allValid = false;
      }
    }
    return allValid ? 2 : 1;
  }
};

int main() {
  const auto classificationCtr = baselib::CountClassification<3>(std::istream_iterator<Passport>(std::cin),
                                                                 std::istream_iterator<Passport>(),
                                                                 [](const Passport& p) { return p.Classify(); });
  fmt::print("{}\n{}\n", classificationCtr.at(1) + classificationCtr.at(2), classificationCtr.at(2));
  return 0;
}
