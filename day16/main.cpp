#include <array>
#include <iostream>
#include <memory_resource>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include <fmt/core.h>

constexpr auto kCols{20};

struct Range {
  int begin;
  int end;

  bool Valid(const int value) const noexcept { return (value >= begin && value < end); }
};

struct Rule {
  std::string name;
  std::array<Range, 2> ranges;

  explicit Rule(std::string_view sv) noexcept {
    size_t len = sv.find(':');
    name       = sv.substr(0, len);
    sv.remove_prefix(len + 2);

    len             = sv.find('-');
    ranges[0].begin = std::stoi(std::string{sv.substr(0, len)});
    sv.remove_prefix(len + 1);
    len           = sv.find(' ');
    ranges[0].end = std::stoi(std::string{sv.substr(0, len)}) + 1;
    sv.remove_prefix(len + 4);

    len             = sv.find('-');
    ranges[1].begin = std::stoi(std::string{sv.substr(0, len)});
    sv.remove_prefix(len + 1);
    ranges[1].end = std::stoi(std::string{sv}) + 1;
  }

  bool Valid(const int value) const noexcept { return ranges[0].Valid(value) || ranges[1].Valid(value); }
};

struct Ticket {
  std::array<int, kCols> vals;

  Ticket() = default;
  explicit Ticket(std::string_view sv) noexcept {
    auto iter{std::begin(vals)};
    while (iter != std::end(vals)) {
      size_t len = sv.find(',');
      if (len == std::string_view::npos) {
        len = sv.size();
      }
      *iter++ = std::stoi(std::string{sv.substr(0, len)});
      sv.remove_prefix(len + 1);
    }
  }
};

int main() {
  const auto [rules, myTicket, tickets] = []() noexcept {
    std::string line{};
    int parseState{0};
    std::vector<Rule> resRules{};
    Ticket resMyTicket{};
    std::vector<Ticket> resTickets{};
    while (std::getline(std::cin, line)) {
      if (line == "" || line == "your ticket:" || line == "nearby tickets:") {
        parseState += 1;
        continue;
      }
      if (parseState == 0) {
        resRules.emplace_back(line);
      } else if (parseState == 2) {
        resMyTicket = Ticket(line);
      } else if (parseState == 4) {
        resTickets.emplace_back(line);
      }
    }
    return std::make_tuple(resRules, resMyTicket, resTickets);
  }();

  const auto [part1, validTickets] = [&]() noexcept {
    size_t res{0};

    std::vector<Ticket> resValidTickets{};

    for (const Ticket& ticket : tickets) {
      bool valid{true};
      for (const int& val : ticket.vals) {
        bool oneRuleValid{false};
        for (const Rule& rule : rules) {
          if (rule.Valid(val)) {
            oneRuleValid = true;
            break;
          }
        }
        if (!oneRuleValid) {
          res += val;
          valid = false;
          break;
        }
      }
      if (valid) {
        resValidTickets.emplace_back(ticket);
      }
    }
    return std::make_tuple(res, resValidTickets);
  }();

  fmt::print("{}\n", part1);

  const size_t part2{[&]() noexcept {
    // TODO: replace with something more efficient
    std::pmr::monotonic_buffer_resource resource;
    std::pmr::unordered_map<int, std::pmr::unordered_set<int>> possibleCols{&resource};

    // TODO: more cache efficient iteration
    for (int iRule{0}; iRule < static_cast<int>(rules.size()); ++iRule) {
      const Rule& rule = rules.at(iRule);
      auto& pCols      = possibleCols[iRule];
      for (int iCol{0}; iCol < kCols; ++iCol) {
        if (std::all_of(std::cbegin(validTickets), std::cend(validTickets), [&](const Ticket& ticket) noexcept {
              return rule.Valid(ticket.vals[iCol]);
            }))
        {
          pCols.emplace(iCol);
        }
      }
    }

    std::array<int, kCols> assignment{};

    while (possibleCols.size() > 0) {
      const auto iter{std::find_if(std::cbegin(possibleCols), std::cend(possibleCols), [](const auto& pCol) noexcept {
        return pCol.second.size() == 1;
      })};
      const int ruleIdx = iter->first;
      const int col     = *std::cbegin(iter->second);
      possibleCols.erase(iter);
      for (auto& [idx, pCol] : possibleCols) {
        pCol.erase(col);
      }
      assignment[ruleIdx] = col;
    }

    size_t result{1};
    for (int iRule{0}; iRule < kCols; ++iRule) {
      if (rules[iRule].name.starts_with("departure")) {
        result *= myTicket.vals[assignment[iRule]];
      }
    }
    return result;
  }()};

  fmt::print("{}\n", part2);

  return 0;
}