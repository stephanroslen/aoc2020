#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <fmt/core.h>

#include <baselib/IntegerCoord.h>

int main() {
  const auto lines{[]() {
    std::vector<std::string> result{};
    std::copy(std::istream_iterator<std::string>{std::cin},
              std::istream_iterator<std::string>{},
              std::back_inserter(result));
    return result;
  }()};

  const auto flipped1{[&]() {
    std::unordered_set<baselib::IntCoord> result{};
    for (const std::string& line : lines) {
      baselib::IntCoord coord{0, 0};
      const size_t size{line.size()};
      for (size_t i{0}; i < size; ++i) {
        const char c = line.at(i);
        if (c == 'w') {
          coord += {-1, 0};
          continue;
        } else if (c == 'e') {
          coord += {1, 0};
          continue;
        }
        ++i;
        const char c2 = line.at(i);
        if (c == 'n') {
          if (c2 == 'w') {
            coord += {-1, 1};
          } else {
            coord += {0, 1};
          }
        } else {
          if (c2 == 'w') {
            coord += {0, -1};
          } else {
            coord += {1, -1};
          }
        }
      }

      const auto iter{result.find(coord)};
      if (iter == result.end()) {
        result.emplace(coord);
      } else {
        result.erase(iter);
      }
    }
    return result;
  }()};

  fmt::print("{}\n", flipped1.size());

  const auto flipped2{[&]() {
    auto result{flipped1};
    for (int i{0}; i < 100; ++i) {
      std::unordered_map<baselib::IntCoord, int> flipNeighCount{};

      constexpr std::array<baselib::IntCoord, 6> offsets{{{-1, 0}, {-1, 1}, {0, 1}, {1, 0}, {1, -1}, {0, -1}}};
      for (const baselib::IntCoord& fTile : result) {
        for (const baselib::IntCoord& offset : offsets) {
          flipNeighCount[fTile + offset] += 1;
        }
      }

      std::unordered_set<baselib::IntCoord> newFlipped{};
      for (const auto& [coord, count] : flipNeighCount) {
        const bool preContained{result.contains(coord)};
        if ((preContained && count > 0 && count <= 2) || (!preContained && count == 2)) {
          newFlipped.emplace(coord);
        }
      }

      result = std::move(newFlipped);
    }
    return result;
  }()};

  fmt::print("{}\n", flipped2.size());

  return 0;
}