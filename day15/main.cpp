#include <iostream>
#include <vector>

#include <fmt/core.h>

int main() {
  const auto input{[]() {
    std::vector<int> result{};
    int tmp{};
    while (std::cin >> tmp) {
      result.emplace_back(tmp);
      std::cin.ignore();
    }
    return result;
  }()};

  const auto solver{[&](int targetNum) {
    std::vector<int> lastOccurence(targetNum, 0);

    int i{0};
    int value{input[0]};

    for (i = 1; i < static_cast<int>(input.size()); ++i) {
      lastOccurence[value] = i;
      value                = input[i];
    }

    for (; i < targetNum; ++i) {
      auto& loc{lastOccurence.at(value)};
      value = (0 == loc) ? 0 : (i - loc);
      loc   = i;
    }
    return value;
  }};

  fmt::print("{}\n", solver(2020));
  fmt::print("{}\n", solver(30000000));

  return 0;
}